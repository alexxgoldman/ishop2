<?php

define("DEBUG", 1); //0 - для пользователей (Произошла ошибка) или 1 - выводит ошибки полностью
define("ROOT", dirname(__DIR__));
define("WWW", ROOT . '/public');
define("APP", ROOT . '/app');
define("CORE", ROOT . '/vendor/ishop/core');
define("LIBS", ROOT . '/vendor/ishop/core/libs');
define("CACHE", ROOT . '/tmp/cache');
define("CONF", ROOT . '/config');
define("LAYOUT", 'watches');

//http://ishop2.loc/public/index.php
$app_path = "http://{$_SERVER['HTTP_HOST']}{$_SERVER["PHP_SELF"]}";

// ищем всё кроме слэша начиня с конца строки
# http://ishop2.loc/public/
$app_path = preg_replace("#[^/]+$#", '', $app_path);

# http://ishop2.loc/
$app_path = str_replace('/public/', '', $app_path);
define("PATH", $app_path);
define("ADMIN", PATH . '/admin');


require_once ROOT . '/vendor/autoload.php';

