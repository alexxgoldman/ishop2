<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 07.03.2018
 * Time: 22:36
 */

namespace app\widgets\menu;


use ishop\App;
use ishop\Cache;
use RedBeanPHP\R;

class Menu{

    protected $data;
    protected $tree;
    protected $menuHtml;
    protected $tpl;
    protected $container = 'ul'; // может быть  select
    protected $table = 'category'; // из какой таблицы берём данные для меню
    protected $cache = 0; // на какое время кэшируем данные
    protected $cacheKey = 'ishop_menu';
    protected $attrs = [];
    protected $prepend = '';

    public function __construct($options = []){
        $this->tpl = __DIR__ . '/menu_tpl/menu.php';
        $this->getOptions($options);
        $this->run();
    }

    protected function getOptions($options){
        foreach ($options as $k => $v) {
            //если такое свойство перечислено в списке выше ($data, $tree и т.д., тогда переприсваем значение)
            if(property_exists($this, $k)){
                $this->$k = $v;
            }
        }
    }

    /**
     * формирует менюшку
     */
    protected function run(){
        $cache = Cache::instance(); // cache это singlton т.е. Нет кэша? Создаём. Есть? Возвращаем.
        $this->menuHtml = $cache->get($this->cacheKey);
        //если данные из кэша не получены
        if(!$this->menuHtml){
            $this->data = App::$app->getProperty('cats');
            if(!$this->data){
                $this->data = R::getAssoc("SELECT * FROM {$this->table}"); // если в кэше ничего нет - достаём из БД
            }
            $this->tree = $this->getTree();
            $this->menuHtml = $this->getMenuHtml($this->tree);
            if($this->cache){
                $cache->set($this->cacheKey, $this->menuHtml, $this->cache);
            }
        }else{
            $this->output();
        }
    }

    protected function output(){
        $attrs = '';
        if(!empty($this->attrs)){
            foreach ($this->attrs as $k => $v){
                $attrs .= " $k='$v' ";
            }
        }
        echo "<{$this->container} class='{$this->class}' $attrs>";
            echo $this->prepend;
            echo $this->menuHtml;
        echo "</{$this->container}>";
    }

    /**
     * из ассоц.массива содаёт дерево
     * то есть элемент будет как бы вложен
     */
    protected function getTree(){
        $tree = [];
        $data = $this->data;
        foreach ($data as $id => &$node) {
            if (!$node['parent_id']){
                $tree[$id] = &$node;
            }else{
                $data[$node['parent_id']]['childs'][$id] = &$node;
            }
        }
        return $tree;
    }

    protected function getMenuHtml($tree, $tab = ''){
        $str = '';
        foreach($tree as $id => $category){
            $str .= $this->catToTemplte($category, $tab, $id);
        }
        return $str;
    }

    protected function catToTemplte($category, $tab, $id){
        ob_start();
        require $this->tpl;
        return ob_get_clean();
    }
}