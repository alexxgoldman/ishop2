<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 07.03.2018
 * Time: 14:02
 */

namespace app\widgets\currency;


use ishop\App;
use RedBeanPHP\R;

class Currency {

    protected $tpl;
    protected $currencies; //в это свойство записываем список всех доступных валют
    protected $currency; // активная валюта

    public function __construct(){
        $this->tpl = __DIR__  . '/currency_tpl/currency.php'; // какой tpl-шаблон будем использовать?
        $this->run(); // сразу вызываем метод run() который вызовет переменные валюты и встроит в html-дерево
    }

    /**
     * получает список валют и получает текущую валюту
     */
    public function run(){
        $this->currencies = App::$app->getProperty('currencies');
        $this->currency = App::$app->getProperty('currency');
        echo $this->getHtml();
    }

    public static function getCurrencies(){
        // getAssoc() берёт ключом первый элемент
        return R::getAssoc("SELECT code, title, symbol_left, symbol_right, value, base 
                                FROM currency
                                ORDER BY base DESC");
    }

    public static function getCurrency($currencies){
        // получает активную валюту
        if(isset($_COOKIE['currency']) && array_key_exists(($_COOKIE['currency']), $currencies)){
            $key = $_COOKIE['currency'];
        }else{
            $key = key($currencies); // key возвращает текущий элемент массива
        }
        $currency = $currencies[$key];
        $currency['code'] = $key;
        return $currency;
    }

    /**
     * формирует HTML-разметку
     */
    protected function getHtml(){
        ob_start();
        require_once $this->tpl;
        return ob_get_clean();
    }

}