<!-- первая по списку - активная валюта -->
<option value="" class="label"><?=$this->currency['code']?></option>
<?php foreach ($this->currencies as $k => $v): ?>
    <!-- активную валюту уже вывели, пропускаем -->
    <?php if($k != $this->currency['code']): ?>
        <option value="<?=$k;?>"><?=$k;?></option>
    <?php endif; ?>
<?php endforeach; ?>
