<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 07.03.2018
 * Time: 17:45
 */

namespace app\controllers;


use app\models\Cart;
use RedBeanPHP\R;

class CurrencyController extends AppController {

    public function changeAction(){
        $currency = !empty($_GET['curr']) ? $_GET['curr'] : null;
        if($currency){
            $curr = R::findOne('currency', "code = ?", [$currency]);
            if (!empty($curr)){
                setcookie('currency', $currency, time()+3600*24*7, '/');
                Cart::recalc($curr);
            }
            //на первом этапе это всё. Страница просто перезагружается. Далее будет пересчёт валют ajax.
        }
        redirect();
    }

}