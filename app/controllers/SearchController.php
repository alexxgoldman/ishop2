<?php

namespace app\controllers;

use RedBeanPHP\R;

class SearchController extends AppController{

    public function typeaheadAction(){
        if($this->isAjax()){
            $query = !empty(trim($_GET['query'])) ? $_GET['query'] : null;
            if($query){
                $products = R::getAll('SELECT id, title FROM product WHERE title LIKE ? LIMIT 9', ["%{$query}%"]);
                //скрипт ожидает что данные $products придут в JSON-формате поэтому:
                echo json_encode($products);
            }
        }
        die;
    }

    public function indexAction(){
        $query = !empty(trim($_GET['s'])) ? $_GET['s'] : null;
        if($query){
            $products = R::find('product', "WHERE title LIKE ?", ["%{$query}%"]);
        }
        $this->setMeta('Поиск по: ' . $query);
        $this->set(compact('products', 'query'));

    }

}