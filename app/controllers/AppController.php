<?php

namespace app\controllers;

use app\models\AppModel;
use app\widgets\currency\Currency;
use ishop\App;
use ishop\base\Controller;
use ishop\Cache;
use RedBeanPHP\R;

class AppController extends Controller {

    public function __construct($route)
    {
        parent::__construct($route);
        new AppModel();
        App::$app->setProperty('currencies', Currency::getCurrencies()); // заносим в реестр чтоб сделать доступным по всему сайту
        App::$app->setProperty('currency', Currency::getCurrency(App::$app->getProperty('currencies')));
        //ложим меню в параметры чтоб иметь постоянный доступ
        App::$app->setProperty('cats', self::cacheCategory());
    }

    public static function cacheCategory(){
        $cache = Cache::instance();
        $cats = $cache->get('cats');
        if(!$cats){
            $cats = R::getAssoc("SELECT * FROM category"); // если в кэше ничего нет - достаём из БД
            $cache->set('cats', $cats); // получили из БД и записываем в кэш
        }
        return $cats;
    }


}