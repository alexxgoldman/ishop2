<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 05.03.2018
 * Time: 23:22
 */

namespace app\controllers;

use ishop\Cache;
use RedBeanPHP\R as R;

class MainController extends AppController {

    /**
     * public $layout = 'test';
     * в каждом контроллере можно задавать свой $layout
     * потому что в общем контоллере прописано свойство $layout
     * которое потом идёт во View и render()
     */


    // в папке Main создать файл index (Как в Yii2)
    public function indexAction(){
        /**
         *  $this->layout = 'test2';
         *  $layout можно прописать и тут, для каждой отдельной страницы
         */
        $brands = R::find('brand', 'LIMIT 3');
        $hits  = R::find('product', "hit ='1' AND status = '1'");
        //debug($brands);
        $this->setMeta("Главная страница", "Описание", "Ключевые слова");
        $this->set(compact('brands', 'hits'));
    }

}