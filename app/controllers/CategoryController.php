<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 15.03.2018
 * Time: 18:05
 */

namespace app\controllers;

use app\models\Breadcrumbs;
use app\models\Category;
use ishop\App;
use ishop\libs\Pagination;
use RedBeanPHP\R;

class CategoryController extends AppController{

    public function viewAction(){
        $alias = $this->route['alias'];
        // информацию о категории извлекаем по алиасу...
        $category = R::findOne('category', 'alias = ?', [$alias]);
        if(!$category){
            throw new \Exception('Страница не найдена', 404);
        }

        //хлебные крошки
        $breadcrumbs = Breadcrumbs::getBreadCrumbs($category->id);

        $cat_model = new Category();
        // ...а подкатегории данной категории разматываем уже передавая id-категории
        $ids = $cat_model->getIds($category->id);
        $ids =  !$ids ? $category->id : $ids . $category->id;

        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $perpage = App::$app->getProperty('pagination');
        $total = R::count('product', "category_id IN ($ids)"); //всего найденных товаров
        $pagination = new Pagination($page, $perpage, $total);
        $start = $pagination->getStart(); // с какой страницы начинать

        $products = R::find('product', "category_id IN ($ids) LIMIT $start, $perpage");
        $this->setMeta($category->title, $category->description, $category->keywords);
        $this->set(compact('products', 'breadcrumbs', 'pagination', 'total'));
    }


}