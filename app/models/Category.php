<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 15.03.2018
 * Time: 18:56
 */

namespace app\models;

use ishop\App;

class Category extends AppModel{

    //передаём id текущей категории
    public function getIds($id){
        $cats = App::$app->getProperty('cats');
        $ids = null;
        foreach ($cats as $k => $v) {
            if($v['parent_id'] == $id){
                $ids .= $k . ','; //$k - это уже будет подкатегория
                $ids .= $this->getIds($k);
            }
        }
        return $ids;
    }

}