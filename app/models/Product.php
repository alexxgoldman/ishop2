<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 08.03.2018
 * Time: 17:31
 */

namespace app\models;


class Product extends AppModel{

    public function setRecentlyViewed($id){
        //Недавно просмотренные товары. В куках храним только номера товаров
        $recentlyViewed = $this->getAllRecentlyViewed();
        if(!$recentlyViewed){
            setcookie('recentlyViewed', $id, time() + 3600*24, '/');
        }else{
            $recentlyViewed = explode('.', $recentlyViewed);//каждый новый просмотренный товар будем добавлять через точку
            if(!in_array($id, $recentlyViewed)){                        //проверяем есть ли в куках этот товар. Если нет - дописываем в массив.
                $recentlyViewed[] = $id;
                $recentlyViewed = implode('.', $recentlyViewed); // соединяем товары через точку (5.4 потом 5.4.8 и т.д.)
                setcookie('recentlyViewed', $recentlyViewed, time() + 3600*24, '/');
            }
            //debug($recentlyViewed);
        }
    }

    public function getRecentlyViewed(){
        if(!empty($_COOKIE['recentlyViewed'])){
            $recentlyViewed = $_COOKIE['recentlyViewed'];
            $recentlyViewed = explode('.', $recentlyViewed);
            return array_slice($recentlyViewed, -3); // берем 3 элемента с конца
        }
        return false;
    }
    public function getAllRecentlyViewed(){
        if(!empty($_COOKIE['recentlyViewed'])){
            return $_COOKIE['recentlyViewed'];
        }
        return false;

    }

}