<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 18.03.2018
 * Time: 12:06
 */

namespace app\models;

use ishop\App;
use RedBeanPHP\R;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class Order extends AppModel {

    public static function saveOrder($data){
        $order = R::dispense('order');//инициируем CRUD с нужной таблицей
        // Часть данных получаем из массива $data
        $order->user_id = $data['user_id']; // записываем данные 1
        $order->note = $data['note']; // записываем данные 2
        // Часть данных получаем из массива $_SESSION
        $order->currency = $_SESSION['cart.currency']['code']; // записываем данные 3
        $order_id = R::store($order);
        self::saveOrderProduct($order_id);
        return $order_id;
    }

    public static function saveOrderProduct($order_id){
        $sql_part = '';
        foreach ($_SESSION['cart'] as $product_id => $product) {
            $product_id = (int)$product_id; // модицикация 1-3 приводится к 1. Распознаётся админом по title.
            $sql_part .= "($order_id, $product_id, {$product['qty']}, '{$product['title']}', {$product['price']}),";
        }
        $sql_part = rtrim($sql_part, ','); //убрать лишнюю запятую в последнем элементе
        R::exec("INSERT INTO order_product (order_id, product_id, qty, title, price) VALUES $sql_part");
    }

    public static function mailOrder($order_id, $user_email){

        // Create the Transport
        $transport = (new Swift_SmtpTransport(App::$app->getProperty('smtp_host'), App::$app->getProperty('smtp_port')))
            ->setUsername(App::$app->getProperty('smtp_login'))
            ->setPassword( App::$app->getProperty('password'))
        ;

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

        // Create a message
        ob_start();
        require APP . '/views/mail/mail_order.php';
        $body = ob_get_clean();

        $message_client = (new Swift_Message("Заказ № {$order_id}"))
            ->setFrom([App::$app->getProperty('smtp_login') => App::$app->getProperty('shop_name')])
            ->setTo($user_email)
            ->setBody($body, 'text/html')
        ;

        $message_admin = (new Swift_Message("Заказ № {$order_id}"))
            ->setFrom([App::$app->getProperty('smtp_login') => App::$app->getProperty('shop_name')])
            ->setTo(App::$app->getProperty('admin_email'))
            ->setBody($body, 'text/html')
        ;

        // Send the message
        $result = $mailer->send($message_client);
        $result = $mailer->send($message_admin);

        // clean Cart after have sent
        unset($_SESSION['cart']);
        unset($_SESSION['cart.qty']);
        unset($_SESSION['cart.sum']);
        unset($_SESSION['cart.currency']);

        $_SESSION['success'] = 'Спасибо за Ваш заказ! В ближайшее время с Вами свяжется менеджер для согласования заказа';
    }

}