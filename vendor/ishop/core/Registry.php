<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 04.03.2018
 * Time: 21:31
 */

namespace ishop;


class Registry {

    //использование Трейта это как код трейта скопировать прямо сюда
    use TSingleton;

    public static $properties = [];

    public function setProperty($name, $value){
        self::$properties[$name] = $value;
    }

    public function getProperty($name){
        if(isset(self::$properties[$name])){
            return self::$properties[$name];
        }
        return null;
    }

    public function getProperties(){
        return self::$properties;
    }

}