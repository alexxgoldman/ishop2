<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 04.03.2018
 * Time: 21:32
 */

namespace ishop;


trait TSingleton {

    private static $instance;

    public static function instance(){
        if(self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

}