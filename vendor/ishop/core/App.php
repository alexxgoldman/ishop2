<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 04.03.2018
 * Time: 21:21
 */

namespace ishop;
//основной класс приложения
class App
{
    // контейнер приложения;
    public static $app;

    public function __construct(){
        // берём запрос пользователя и обрезаем слэш
        $query = trim($_SERVER["QUERY_STRING"], '/');
        //стартуем сессию
        session_start();
        //запишем в $app объект класса Registry
        self::$app = Registry::instance();
        $this->getParams();
        // подключаем класс обработки ошибок
        new ErrorHandler();
        // передаём запрошенный адрес
        Router::dispatch($query);
    }

    protected function getParams(){
        //теперь в $params находится массив который возвращает файл params.php
        $params = require_once CONF . '/params.php';
        if(!empty($params)){
            foreach ($params as $k => $v) {
                self::$app->setProperty($k, $v);
            }
        }
    }
}