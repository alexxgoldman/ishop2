<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 06.03.2018
 * Time: 22:58
 */

namespace ishop\base;


use ishop\Db;
use RedBeanPHP\R;
use Valitron\Validator;

abstract class Model {

    public $attributes = [];
    public $errors = [];
    public $rules = [];

    /**
     * инициируем подключение к БД
     */
    public function __construct(){
        Db::instance();
    }

    public function load($data){
        //load($_POST[])
        //  $_POST['name'] = 'Vasya'
        //  $_POST['pass'] = '123'
        foreach ($this->attributes as $name => $value) {
            if(isset($data[$name])){
                $this->attributes[$name] = $data[$name];
             // $attributes['login'] = $_POST['login'] = 'Vasya'
             // $attributes['password'] = $_POST['password'] = '123'
            }
        }
    }

    public function validate($data){
        Validator::langDir(WWW . '/validator/lang');
        Validator::lang('uk'); /* перевод валидации (можно поправить) /valitron/lang/ru.php */
        $v = new Validator($data);
        $v->rules($this->rules);
        if($v->validate()){
            return true;
        }
        $this->errors = $v->errors();
        return false;
    }

    public function save($table){
        $tbl = R::dispense($table);
        foreach ($this->attributes as $name => $value) {
            $tbl->$name = $value;
        }
        return R::store($tbl);
    }

    public function getErrors(){
        $errors = '<ul>';
        foreach ($this->errors as $error) {
            foreach ($error as $item) {
                $errors .= "<li>$item</li>";
            }
        }
        $errors .= '</ul>';
        $_SESSION['error'] = $errors;
    }

}