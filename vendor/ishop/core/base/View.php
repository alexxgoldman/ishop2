<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 06.03.2018
 * Time: 0:44
 */

namespace ishop\base;


class View {

    public $route;
    public $controller;
    public $view;
    public $model;
    public $prefix;
    public $layout;
    public $data = []; //потом все свойства (переменные) из этого массива рендерятся в виде
    public $meta = [];

    public function __construct($route, $layout = '', $view = '', $meta){
        $this->route = $route;
        $this->controller = $route['controller'];
        $this->view = $view;
        $this->model = $route['controller']; //модель имеет то же имя, что и контроллер
        $this->prefix = $route['prefix'];
        $this->meta = $meta;
        /**
         * $layout может вообще не использоваться, например при ответе ajax-запроса
         */
        if($layout === false){
            $this->layout = false;
        }else{
            $this->layout = $layout ? : LAYOUT;
        }
    }

    public function render($data){
        /**
         * сейчас $data это массив. Но в виде удобно использовать просто переменные
         * Поэтому применяем фуцнкцию extract()
         */
        if (is_array($data)) extract($data);
        //формируем файл вида в соответствии с набором параметров (= запросом)
       $viewFile = APP . "/views/{$this->prefix}{$this->controller}/{$this->view}.php";
       // если название файла, что сформировано выше таки существует ...
       if(is_file($viewFile)){
            ob_start();
           /**
            * то подключаем его.
            * Но так вид будет выводиться сразу, без <!doctype> и т.д.
            * Поэтому включаем ob_start()
            */
            require_once $viewFile;
            $content = ob_get_clean(); // в $content сейчас возвращена вся страница (echo - отображает её)
       }else{
           // если не существует - выбрасываем исключение
           throw new \Exception("Не найден вид {$viewFile}", 500);
       }

       // шаблон подключаем только если $layout не стоит в false (для ajax - $layout не нужен будет)
       if(false !== $this->layout){
           /**
            * формируем путь к шаблону для конкретной страницы.
            * Шаблон по умолчанию тут: define("LAYOUT", ROOT . 'default');
            */
              $layoutFile = APP . "/views/layouts/{$this->layout}.php";
             if (is_file($layoutFile)){
                 require_once $layoutFile;
             }else{
                 throw new \Exception("Не найден шаблон {$this->layout}", 500);
             }
       }
    }

    public function getMeta(){
        $output = '<title>' . $this->meta['title'] . '</title>' . PHP_EOL;
        $output .= '<meta name="description" content="'. $this->meta['desc'] .'">' . PHP_EOL;
        $output .= '<meta name="keywords" content="'. $this->meta['keywords'] .'">' . PHP_EOL;
        return $output;
    }

}