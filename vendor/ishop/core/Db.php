<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 06.03.2018
 * Time: 23:06
 */

namespace ishop;

use \RedBeanPHP\R as R;

class Db {

    use TSingleton;

    protected function __construct() {
        $db = require_once CONF . '/config_db.php';
        R::setup($db['dsn'], $db['user'], $db['pass']);
        if(!R::testConnection()){
            throw new \Exception("Нет соединения с БД", 500);
        }/*else{
            echo 'Соединение установлено!'; закомментировал потому что выдавало ошибку headers already sent при работе с меню и куками
        }*/

        R::freeze(true);
        if(DEBUG){
            R::debug(true, 1);
        }
    }

}