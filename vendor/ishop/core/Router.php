<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 05.03.2018
 * Time: 21:13
 */

namespace ishop;

class Router {

    //сюда записываем марштуры у нас на сайте
    protected static $routes = [];
    // сюда записываем текующий маршрут если найдено соответствие запрошенного адреса с каким то адресом в таблице марштутов
    protected static $route = [];

    public static function add($regexp, $route = []) {
        self::$routes[$regexp] = $route;
    }

    public static function getRoutes(){
        return self::$routes;
    }

    // возвращает текущий маршрут
    public static function getRoute(){
        return self::$route;
    }

    public static function dispatch($url){
        $url = self::removeQueryString($url);
        if (self::matchRoute($url)){
            //вызываем только те классы которые содержат в своём написании постфикс `Controller`
           $controller = 'app\controllers\\' . self::$route['prefix'] . self::$route['controller'] . 'Controller';
           if (class_exists($controller)){
               //в контроллер передаём текующий маршрут. В текущем машруте всё содержится: и имя контроллера и имя экшена и имя префикса
                $controllerObject = new $controller(self::$route);
                $action = self::lowerCamelCase(self::$route['action']) . 'Action';
               if (method_exists($controllerObject, $action)){
                    $controllerObject->$action(); // вызываем нужный метод
                    $controllerObject->getView(); // вызывается метод getView базового контроллера
               }else{
                   throw new \Exception("Метод $controller::$action не найден", 404);
               }
           }else{
                throw new \Exception("Контроллер $controller не найден", 404);
           }
        }else{
            // если страница не найдена
            throw new \Exception("Странциа не найдена", 404);
        }
    }

    //true или false
    public static function matchRoute($url){
        foreach (self::$routes as $pattern => $route) {
            //если мы нашли первое подходящее правило ...
            if (preg_match("#{$pattern}#i", $url, $matches)){
                foreach ($matches as $k => $v) {
                    if (is_string($k)){
                        $route[$k] = $v;
                    }
                }
                //если нет action, то ставим по умолчанию index
                if(empty($route['action'])){
                    $route['action'] = 'index';
                }
                if(!isset($route['prefix'])){
                    $route['prefix'] = '';
                } else {
                    $route['prefix'] .= '\\';
                }
                $route['controller'] = self::upperCamelCase($route['controller']);
                self::$route = $route;
                // ... то выходим  и возвращаем true
                return true;
            }
        }
        return false;
    }

    //тут изменяем имена контроллеров CamelCase
    protected static function upperCamelCase($name){
        //1.например будет вот так: http://ishop2.loc/page-new/. Ищем дефис "-", заменяем на пробел " " | str_replace('-', ' ', $name)
        //2. ucwords делает из  Page new => Page New | ucwords(str_replace('-', ' ', $name))
        //3. И третьим шагом опять вызываем str_replace, ищем пробел и заменяем на пустую строку
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $name)));
    }
    //тут изменяем имена action
    protected static function lowerCamelCase($name){
        /**
         * 1. upperCamelCase => PageController
         * 2. lcfirst => pageController
         */
        return lcfirst(self::upperCamelCase($name));
    }

    protected static function removeQueryString($url){
        if($url){
            /**
             *
            [0] => page/view/
            [1] => id=1&page=2&name=Alexx
             */
            $params = explode('&', $url, 2);

            if(false === strpos(($params[0]), '=')){
                return rtrim($params[0], '/');
            }else{
                return '';
            }
        }
    }

}