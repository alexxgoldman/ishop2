<?php

// заменил print_r на var_dump чтоб было и красиво и более информативно
function debug($arr){
    echo '<pre>'; print_r($arr); echo '</pre>';
}

function redirect($http = false){
    if($http){
        $redirect = $http;
    }else{
        // если пользователь ничего не передал это значит, что он хочет просто перезапросить страницу
        $redirect = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : PATH;
    }
    header("Location:".$redirect);
    exit;
}

/**
 * @param $str
 * @return string
 * обёртка для htmlspecialchars
 */
function h($str){
    return htmlspecialchars($str, ENT_QUOTES);
}

