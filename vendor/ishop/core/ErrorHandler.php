<?php
/**
 * Created by PhpStorm.
 * User: veles
 * Date: 05.03.2018
 * Time: 20:09
 */

namespace ishop;


class ErrorHandler
{
// в классе ошибок сразу инициализируем режим отображения ошибок.
// А для этого проверяем в каком режиме мы сейчас находимся DEBUG = 1 разработка /0 боевой
    public function __construct(){
        if (DEBUG){
            error_reporting(-1);
        } else {
            error_reporting(0);
        }
        //для обработки ошибок передадим СВОЮ функцию exceptionHandler которую пропишем здесь же($this)
        set_exception_handler([$this, 'exceptionHandler']);
    }

    public function exceptionHandler($e){
        $this->logErrors($e->getMessage(), $e->getFile(), $e->getLine());
        $this->displayError('Исключение', $e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode());
    }

    protected function logErrors($message = '', $file = '', $line = ''){
        error_log("[" . date('Y-m-d H:i:s') . "], Текст ошибки: {$message} | Файл: {$file} | Строка {$line}\n==================\n", 3, ROOT . '/tmp/errors.log');
    }

    // ошибки: номер, строка, файл, строка, ответ серверу (по умолчанию 404)
    protected function displayError($errno, $errstr, $errfile, $errline, $response = 404){
        http_response_code($response);
        //шаблон 404 ошибки показываем только если сам ответ = 404 и если константа DEBUG выставлена в 0
        if ($response == 404  && !DEBUG){
            require WWW . '/errors/404.php';
            die;
            // можно подключать нужный шаблон автоматически правильно присвоив имена файлам 404.php, 500.php => $response.'.php'
        }
        if (DEBUG) {
            require WWW . '/errors/dev.php';
        } else {
            require WWW . '/errors/prod.php';
        }
    }

}